// require('dotenv').config()
// const Express =require('Express')
// const bodyParser=require('body-parser')
// const axios=require('axios')
// const ChatGPTAPI=require('chatgpt')
// import dotenv from 'dotenv'
// import { Express } from 'Express'
// import * as dotenv from 'dotenv' // see https://github.com/motdotla/dotenv#how-do-i-use-dotenv-with-import
// dotenv.config()
import dotenv from 'dotenv'
dotenv.config()
import Express from 'express';
import bodyParser from 'body-parser'
import axios from 'axios'
import { ChatGPTAPI,ChatGPTUnofficialProxyAPI } from 'chatgpt'
import fetch from 'node-fetch'
import {Configuration,OpenAIApi} from 'openai'

export { fetch }

// const {TOKEN,SERVER_URL}=process.env
const TELEGRAM_API=`https://api.telegram.org/bot${process.env.TOKEN}`
const URI=`/webhook/${process.env.TOKEN}`
const WEBHOOK_URL=process.env.SERVER_URL+URI
//const SESSION_TOKEN = process.env.OPEN_AI_SESSION_TOKEN;
// const OPEN_AI_SESSION_TOKEN='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'

const app=Express()
app.use(bodyParser.json())

const init = async()=>{
    const res=await axios.get(`${TELEGRAM_API}/setWebhook?url=${WEBHOOK_URL}`)
    console.log(res.data)
}

app.post(URI, async(req,res)=>{
    // console.log(req.body)

    // const chatId = req.body.message.chat.id
    // const text = req.body.message.text
    // await axios.post(`${TELEGRAM_API}/sendMessage`,{
    //     chat_id:chatId,
    //     text:text
    // })

    // return res.send()
     const chatId = req.body.message.chat.id;
	 const validtext = req.body.message.text;
	 const args = validtext.split(' ');
  	const command = args.shift().toLowerCase();
   // const chatId='6070180576'

    try {
		// Console.log the incoming message
		console.log(req.body);
		console.log(command+"log")

		// Get chatGPT response by sending message.text
		const chatGptRes = await chatGpt(req.body.message);

		// Take the chatGPT response and send it to the telegram bot

		// if (validtext === '/test1') {
		// 	await axios.post(`${TELEGRAM_API}/sendMessage`, {
		// 		chat_id: chatId,
		// 		text: text
		// 	})
		// 	return res.send();
			
		// }else{
		await axios.post(`${TELEGRAM_API}/sendMessage`, {
			chat_id: chatId,
			text: chatGptRes.text
		})
		return res.send();
	//}
	} catch (error) {
		console.log('Error: ', error);
			// Ping the user that error has occured.
		await axios.post(`${TELEGRAM_API}/sendMessage`, {
			chat_id: chatId,
			text: 'An Error Occurred!'
		});
		return res.send();
	}
})

const chatGpt = async (message) => {
    // const api = new ChatGPTAPI({
    //     apiKey: process.env.OPENAI_API_KEY
    //   })

    
	try {
        const api = new ChatGPTAPI({
            apiKey: process.env.OPENAI_API_KEY
          })
		// intialise the new ChatGPTAPI using session token
		// const api = new ChatGPTAPI({ apiKey: process.env.OPENAI_API_KEY })
		//await api.ensureAuth();
		// await response from chatGPT by passing in message.text as prompt
		const response = await api.sendMessage(message.text)
		console.log(response);
		return Promise.resolve(response);
	} catch (err) {
		console.log('Error in chatGPT: ', err)
		return Promise.reject(err);
	}
}

app.listen(process.env.PORT || 5000,async()=>{
    console.log('App run on Port',process.env.PORT || 5000)
    await init()
})